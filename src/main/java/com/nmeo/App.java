package com.nmeo;

import io.javalin.Javalin;

import org.apache.commons.lang3.EnumUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.dto.Pokemon;
import com.nmeo.dto.SearchResponse;
import com.nmeo.models.PokemonType;
import com.nmeo.pokedex.PokedexManager;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class.getName());
    public static void main(String[] args) {
        Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ALL);
        logger.info("Pokedex backend is booting...");

        int port = System.getenv("SERVER_PORT") != null? Integer.parseInt(System.getenv("SERVER_PORT")) : 8080;

        PokedexManager pokedex = new PokedexManager();

        Javalin.create()
        .get("/api/status", ctx -> {
            logger.debug("Status handler triggered", ctx);
            ctx.status(200);
        })
        .post("/api/create", ctx -> {
            Pokemon pokemon = ctx.bodyAsClass(Pokemon.class);
            if (pokedex.createPokemon(pokemon)) {
                ctx.status(200);
            }
            else {
                ctx.status(400);
            }
        })
        .get("/api/searchByName", ctx -> {
            String nameToSearch = ctx.queryParam("name");
            SearchResponse response = new SearchResponse(pokedex.searchPokemonsByName(nameToSearch));
            ctx.status(200);
            ctx.json(response);
        })
        .get("/api/searchByType", ctx -> {
            String typeToSearch = ctx.queryParam("type");
            if (EnumUtils.isValidEnum(PokemonType.class, typeToSearch)) {
                SearchResponse response = new SearchResponse(pokedex.searchPokemonsByType(PokemonType.valueOf(typeToSearch)));
                ctx.status(200);
                ctx.json(response);
            }
            else {
                ctx.status(400);
            }
        })
        .post("/api/modify", ctx -> {
            ModifyPokemonRequest request = ctx.bodyAsClass(ModifyPokemonRequest.class);
            if (pokedex.modifyPokemon(request)) {
                ctx.status(200);
            }
            else {
                ctx.status(404);
            }
        })
        .start(port);
    }
}