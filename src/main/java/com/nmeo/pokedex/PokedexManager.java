package com.nmeo.pokedex;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.nmeo.dto.ModifyPokemonRequest;
import com.nmeo.dto.Pokemon;
import com.nmeo.models.PokemonType;

public class PokedexManager {
    private Set<Pokemon> pokemons = new HashSet<>();

    public boolean createPokemon(Pokemon pokemon) {
        return pokemons.add(pokemon);
    }

    public List<Pokemon> searchPokemonsByName(String name) {
        return pokemons.stream()
            .filter(pokemon -> pokemon.pokemonName.contains(name))
            .collect(Collectors.toList());
    }

    public List<Pokemon> searchPokemonsByType(PokemonType type) {
        return pokemons.stream()
            .filter(pokemon -> pokemon.type.equals(type))
            .collect(Collectors.toList());
    }

    public boolean modifyPokemon(ModifyPokemonRequest req) {
        List<Pokemon> pokList = searchPokemonsByName(req.pokemonName);
        if (pokList.isEmpty()) return false;
        for (Pokemon p : pokList) {
            if (p.pokemonName.equals(req.pokemonName)) {
                p.lifePoints = req.lifePoints == null ? p.lifePoints : req.lifePoints;
                p.type = req.type == null ? p.type : req.type;
                if (req.powers != null) p.powers.addAll(req.powers);
                return true;
            }
        }
        return false;
    }
}
