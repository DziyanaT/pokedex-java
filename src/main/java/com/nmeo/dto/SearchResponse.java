package com.nmeo.dto;

import java.util.List;

public class SearchResponse {
    public List<Pokemon> result;

    public SearchResponse(List<Pokemon> res) {
        result = res;
    }
}
