package com.nmeo.dto;

import java.util.ArrayList;

import com.nmeo.models.PokemonType;

public class Pokemon {
    public String pokemonName;
    public PokemonType type;
    public Integer lifePoints;
    public ArrayList<Power> powers;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((pokemonName == null) ? 0 : pokemonName.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pokemon other = (Pokemon) obj;
        if (pokemonName == null) {
            if (other.pokemonName != null)
                return false;
        } else if (!pokemonName.equals(other.pokemonName))
            return false;
        return true;
    }

    
}