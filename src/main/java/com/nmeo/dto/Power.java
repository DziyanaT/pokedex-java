package com.nmeo.dto;

import com.nmeo.models.PokemonType;

public class Power {
    public String powerName;
    public PokemonType damageType;
    public int damage;
}